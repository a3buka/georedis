package geo

import (
	geo "github.com/kellydunn/golang-geo"
	"github.com/robertkrimen/otto"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name PolygonChecker
type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func (p *Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}

func (p *Polygon) RandomPoint() Point {

	rand.Seed(time.Now().UnixNano())

	minLat := 59.81002495078666
	maxLat := 60.0954811436399
	minLng := 30.14495968779295
	maxLng := 30.553621053301985
	for {
		randLat := minLat + (maxLat-minLat)*rand.Float64()
		randLng := minLng + (maxLng-minLng)*rand.Float64()
		if p.Contains(Point{Lat: randLat, Lng: randLng}) {
			return Point{Lat: randLat, Lng: randLng}
		}
	}

	//return Point{Lat: randLat, Lng: randLng}
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	var geoPoints []*geo.Point
	for _, point := range points {
		geoPoints = append(geoPoints, geo.NewPoint(point.Lat, point.Lng))
	}

	return &Polygon{
		polygon: geo.NewPolygon(geoPoints),
		allowed: allowed,
	}
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	// проверить, находится ли точка в разрешенной зоне
	if allowedZone.Contains(point) {
		for _, zone := range disabledZones {
			if zone.Contains(point) {
				return false
			}
		}
		return true
	}
	return false
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var randomPoint Point
	// получение случайной точки в разрешенной зоне
	for {
		randomPoint = allowedZone.RandomPoint()
		if CheckPointIsAllowed(randomPoint, allowedZone, disabledZones) {
			break
		}
	}
	return randomPoint
}

func NewDisAllowedZone1() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := Parse("noOrdersPolygon2", 1)
	return NewPolygon(points, false)
}

func NewDisAllowedZone2() *Polygon {

	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := Parse("noOrdersPolygon2", 2)
	return NewPolygon(points, false)
}

func NewAllowedZone() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := Parse("mainPolygon", 0)

	return NewPolygon(points, true)
}

func Parse(name string, index int) []Point {
	vm := otto.New()

	bytes, err := os.ReadFile("./public/js/polygon.js")
	if err != nil {
		log.Fatal(err)
	}
	stringSplit := strings.Split(string(bytes), ";")
	//fmt.Println(stringSplit[0])
	//Распарсили polygon.js
	_, _ = vm.Run(stringSplit[index])
	//Получили значение в виде строки
	fl, _ := vm.Get(name)
	//Разделим строку на координаты
	atf := strings.Split(fl.String(), ",")

	var points []Point                      // Если усовершенстовать можно сделать через мейк
	for i := 0; i < len(atf)-1; i = i + 2 { // шаг 2 так как 1 значение долгота 2 широта - объединяем
		lat, _ := vm.ToValue(atf[i]) //байты в значение
		latFloat, _ := lat.ToFloat() //значение в флоат
		lng, _ := vm.ToValue(atf[i+1])
		lngFloat, _ := lng.ToFloat()

		points = append(points, Point{Lat: latFloat, Lng: lngFloat})
	}
	return points
}
