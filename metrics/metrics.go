package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

type Metrics struct {
	Counter       prometheus.Gauge
	PespawnOrders prometheus.Gauge
	GetStatus     prometheus.Histogram
	Move          prometheus.Histogram
}

func NewMetrics() *Metrics {

	counter := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "courier",
			Name:      "courier_counter",
			Help:      "total Number of request",
		})

	historhamGetStatus := prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:      "histogram_status",
			Namespace: "get_status",
		})
	historgamMove := prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:      "histogram_move",
			Namespace: "move",
		})

	respawnOrders := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "orders",
			Name:      "near_orders",
			Help:      "respawn orders near courier",
		})
	prometheus.MustRegister(counter, respawnOrders, historgamMove, historhamGetStatus)
	return &Metrics{Counter: counter, PespawnOrders: respawnOrders, GetStatus: historhamGetStatus, Move: historgamMove}
}
