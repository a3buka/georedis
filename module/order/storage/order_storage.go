package storage

import (
	"context"
	"github.com/jmoiron/sqlx"
	geo "github.com/kellydunn/golang-geo"
	"gitlab.com/a3buka/georedis/module/order/models"
	"log"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)          // получить заказ по id
	//GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	db *sqlx.DB
}

func NewOrderStorage(db *sqlx.DB) OrderStorager {
	query := `CREATE TABLE IF NOT EXISTS orders (
    id SERIAL PRIMARY KEY,
    price FLOAT,
    delivery_price FLOAT,
    lng FLOAT,
    lat FLOAT,
    created_at TIME);`
	_, err := db.Exec(query)
	if err != nil {
		log.Fatal(err)
	}

	return &OrderStorage{db: db}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	query := `INSERT INTO orders (price, delivery_price, lng, lat, created_at)
VALUES ($1,$2,$3,$4,$5)`
	_, err := o.db.Exec(query, order.Price, order.DeliveryPrice, order.Lng, order.Lat, time.Now())
	if err != nil {
		log.Printf("save err: %s", err)
		return err
	}
	return nil
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	query := `DELETE FROM orders WHERE created_at < $1`
	_, err := o.db.Exec(query, time.Now().Add(-maxAge))
	if err != nil {
		log.Printf("RemoveOld err: %s", err)
		return err
	}
	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	order := &models.Order{}
	query := `SELECT (*) FROM orders WHERE id = $1 `
	err := o.db.Get(order, query, orderID)
	if err != nil {
		log.Printf("GetById err: %s", err)
		return nil, err
	}
	return order, nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	var count int
	query := `SELECT count(*) FROM orders`
	err := o.db.Get(&count, query)
	if err != nil {
		log.Printf("Get count err: %s", err)
		return 0, err
	}
	return count, nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {

	orders := []models.Order{}
	query := `SELECT * FROM orders`
	err := o.db.Select(&orders, query)
	if err != nil {
		log.Printf("GetByRadius err: %s", err)
		return nil, err
	}

	res := make([]models.Order, 0, len(orders))
	for i, idx := range orders {
		polygonStandart := geo.NewPoint(lat, lng)
		polygonOrders := geo.NewPoint(idx.Lat, idx.Lng)
		distRadius := polygonStandart.GreatCircleDistance(polygonOrders)
		if distRadius < radius/1000 {
			res = append(res, orders[i])
		}
	}

	return res, nil
}
