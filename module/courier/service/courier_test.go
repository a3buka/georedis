package service

import (
	"context"
	"gitlab.com/a3buka/georedis/geo"
	"gitlab.com/a3buka/georedis/module/courier/models"
	"gitlab.com/a3buka/georedis/module/courier/storage/mocks"
	"reflect"
	"testing"
)

func TestCourierService_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "Get Courier",
			fields: fields{courierStorage: mocks.NewCourierStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args:    args{ctx: context.Background()},
			want:    &models.Courier{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("GetCourier", tt.args.ctx).Return(tt.want, nil)
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Move Courier",
			fields: fields{courierStorage: mocks.NewCourierStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{courier: models.Courier{},
				direction: 0,
				zoom:      14,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//tt.fields.courierStorage.On("MoveCourier", tt.args.courier, tt.args.direction, tt.args.zoom).Return(nil)
			tt.fields.allowedZone.On("Contains", geo.Point{Lat: 0.0001}).Return(true)
			//tt.fields.allowedZone.On("RandomPoint").Return(geo.Point{})
			tt.fields.courierStorage.On("Save", context.Background(), models.Courier{Score: 0, Location: models.Point{Lat: 0.0001}}).Return(nil)
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := c.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
