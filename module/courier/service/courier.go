package service

import (
	"context"
	"gitlab.com/a3buka/georedis/geo"
	"gitlab.com/a3buka/georedis/module/courier/models"
	"gitlab.com/a3buka/georedis/module/courier/storage"
	"log"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name Courierer
type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	var courier *models.Courier
	courier, err := c.courierStorage.GetOne(ctx)

	if courier == nil {
		courier = new(models.Courier)
		courier.Location.Lng = DefaultCourierLng
		courier.Location.Lat = DefaultCourierLat
		err = nil
	}
	if err != nil {
		log.Printf("GetCourier %s", err)
		return nil, err
	}

	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	point := models.Point{Lat: courier.Location.Lat, Lng: courier.Location.Lng}
	//if !c.allowedZone.Contains(geo.Point(point)) {
	if !geo.CheckPointIsAllowed(geo.Point(point), c.allowedZone, c.disabledZones) {
		newPoint := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat = newPoint.Lat
		courier.Location.Lng = newPoint.Lng
	}
	// сохраняем новые координаты курьера
	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		log.Printf("Save get Courier %s", err)

		return nil, err
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	accuracy := 0.0001 / math.Pow(2, float64(zoom-14))
	switch direction {
	case DirectionUp:
		courier.Location.Lat += accuracy
	case DirectionDown:
		courier.Location.Lat -= accuracy
	case DirectionLeft:
		courier.Location.Lng -= accuracy
	case DirectionRight:
		courier.Location.Lng += accuracy

	}
	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	if !c.allowedZone.Contains(geo.Point{Lat: courier.Location.Lat, Lng: courier.Location.Lng}) {
		newPoint := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat = newPoint.Lat
		courier.Location.Lng = newPoint.Lng
	}

	// далее сохранить изменения в хранилище
	err := c.courierStorage.Save(context.Background(), courier)
	if err != nil {
		log.Printf("Save MoveCourier: %s", err)
	}
	return nil
}
