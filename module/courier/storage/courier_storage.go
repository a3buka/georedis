package storage

import (
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/georedis/module/courier/models"
	"log"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	db *sqlx.DB
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	//var count int
	//querySELECT := `SELECT COUNT(*) FROM couriers`
	//err := c.db.Get(&count, querySELECT)
	//if err != nil {
	//	log.Print(err)
	//	return err
	//}
	//if count == 0 {
	//	query := `INSERT INTO couriers (id, score, lat, lng) VALUES ($1, $2, $3, $4)`
	//	_, err = c.db.Exec(query, 0, courier.Score, courier.Location.Lat, courier.Location.Lng)
	//	if err != nil {
	//		log.Print(err)
	//		return err
	//	}
	//} else {
	//	query := `UPDATE couriers SET score = $2, lat = $3, lng = $4 WHERE id = $1;`
	//	_, err = c.db.Exec(query, 0, courier.Score, courier.Location.Lat, courier.Location.Lng)
	//	if err != nil {
	//		log.Print(err)
	//		return err
	//	}
	//}
	query := `INSERT INTO couriers VALUES ($1, $2, $3, $4) ON CONFLICT (id) DO UPDATE SET score = $2, lat = $3, lng = $4`

	courierId := 0

	_, err := c.db.Exec(query, courierId, courier.Score, courier.Location.Lat, courier.Location.Lng)
	if err != nil {
		return err
	}

	return nil

	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	res := &models.Courier{}

	query := `SELECT id, score, lat as "location.lat", lng as "location.lng" FROM couriers LIMIT 1`

	err := c.db.Get(res, query)
	if err != nil {
		return nil, err
	}

	return res, err
}

func NewCourierStorage(db *sqlx.DB) CourierStorager {
	query := `CREATE TABLE IF NOT EXISTS couriers (
    id INT,
    score INT,
    lng FLOAT,
    lat FLOAT,
    CONSTRAINT uc_couriers_id UNIQUE (id)); `
	_, err := db.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
	return &CourierStorage{db: db}
}
