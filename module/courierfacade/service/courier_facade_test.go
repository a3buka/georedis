package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	models2 "gitlab.com/a3buka/georedis/module/courier/models"
	amocks "gitlab.com/a3buka/georedis/module/courier/service/mocks"
	models3 "gitlab.com/a3buka/georedis/module/order/models"

	"gitlab.com/a3buka/georedis/module/courierfacade/models"
	omocks "gitlab.com/a3buka/georedis/module/order/service"
	"reflect"
	"testing"
)

func TestCourierFacade_GetStatus(t *testing.T) {
	type fields struct {
		courierService *amocks.Courierer
		orderService   *omocks.MockOrderer
	}
	type args struct {
		ctx     context.Context
		courier models.CourierStatus
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "Get status",
			fields: fields{
				courierService: amocks.NewCourierer(t),
				orderService:   omocks.NewOrderer(t),
			},
			args: args{ctx: context.Background()},
			want: models.CourierStatus{
				Courier: models2.Courier{},
				Orders:  []models3.Order{},
			},
		},
	}
	for _, tt := range tests {
		tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&models2.Courier{}, nil)
		tt.fields.orderService.On("GetByRadius", tt.args.ctx, float64(0), float64(0), mock.Anything, "m").Return([]models3.Order{}, nil)
		t.Run(tt.name, func(t *testing.T) {
			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {
	type fields struct {
		courierService *amocks.Courierer
		orderService   *omocks.MockOrderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Move Courier",
			fields: fields{
				courierService: amocks.NewCourierer(t),
				orderService:   omocks.NewOrderer(t),
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      14,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&models2.Courier{}, nil)
			tt.fields.courierService.On("MoveCourier", models2.Courier{}, tt.args.direction, tt.args.zoom).Return(nil)

			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
