package service

import (
	"context"
	cservice "gitlab.com/a3buka/georedis/module/courier/service"
	cfm "gitlab.com/a3buka/georedis/module/courierfacade/models"
	oservice "gitlab.com/a3buka/georedis/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
	deliveryRadius          = 5    //5m
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierFacer
type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	modelCourier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println("facade move GET")
		return
	}
	err = c.courierService.MoveCourier(*modelCourier, direction, zoom)
	if err != nil {
		log.Println("facade move")
		return
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	modelCourier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return cfm.CourierStatus{}
	}

	orders, err := c.orderService.GetByRadius(
		ctx,
		modelCourier.Location.Lng,
		modelCourier.Location.Lat,
		CourierVisibilityRadius,
		"m")
	if err != nil {
		log.Println("facade GetStatus")
		return cfm.CourierStatus{}
	}
	//Получение заказа в радиусе
	//deliviry, err := c.orderService.GetByRadius(
	//	ctx,
	//	modelCourier.Location.Lng,
	//	modelCourier.Location.Lat,
	//	deliveryRadius,
	//	"m")
	//for _, idx := range deliviry {
	//	modelCourier.Score++

	//}
	res := cfm.CourierStatus{Courier: *modelCourier, Orders: orders}
	return res
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}
