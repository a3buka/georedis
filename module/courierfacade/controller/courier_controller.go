package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/a3buka/georedis/metrics"
	"gitlab.com/a3buka/georedis/module/courierfacade/service"
	"log"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
	metrics        *metrics.Metrics
}

func NewCourierController(courierService service.CourierFacer, metrics *metrics.Metrics) *CourierController {
	return &CourierController{courierService: courierService, metrics: metrics}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	//считаем использование
	c.metrics.Counter.Inc()
	// Начало отчета для гистограмы
	start := time.Now()

	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)

	// считаем заказы
	c.metrics.PespawnOrders.Set(float64(len(status.Orders)))
	// отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)

	defer func() {
		duration := time.Since(start).Seconds()
		c.metrics.GetStatus.Observe(duration)
	}()

}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	//считаем метрики
	c.metrics.Counter.Inc()
	start := time.Now()
	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err = json.Unmarshal((m.Data).([]byte), &cm)
	if err != nil {
		log.Printf("Courier controller json: %s", err)
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
	defer func() {
		duration := time.Since(start).Seconds()
		c.metrics.Move.Observe(duration)
	}()
}
