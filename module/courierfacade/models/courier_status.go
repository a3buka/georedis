package models

import (
	cm "gitlab.com/a3buka/georedis/module/courier/models"
	om "gitlab.com/a3buka/georedis/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
