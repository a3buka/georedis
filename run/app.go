package run

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/a3buka/georedis/geo"
	"gitlab.com/a3buka/georedis/internal/db"
	"gitlab.com/a3buka/georedis/metrics"
	cservice "gitlab.com/a3buka/georedis/module/courier/service"
	cstorage "gitlab.com/a3buka/georedis/module/courier/storage"
	"gitlab.com/a3buka/georedis/module/courierfacade/controller"
	cfservice "gitlab.com/a3buka/georedis/module/courierfacade/service"
	oservice "gitlab.com/a3buka/georedis/module/order/service"
	ostorage "gitlab.com/a3buka/georedis/module/order/storage"
	"gitlab.com/a3buka/georedis/router"
	"gitlab.com/a3buka/georedis/server"
	"gitlab.com/a3buka/georedis/workers/order"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {

	dbx := db.NewSqlDB()
	defer dbx.Close()

	metric := metrics.NewMetrics()

	// Чтобы все отчищало в редис
	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(dbx)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(dbx)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade, metric)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
