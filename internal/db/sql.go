package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"os"
)

func NewSqlDB() *sqlx.DB {
	var dsn string

	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	pass := os.Getenv("DB_PASSWORD")

	dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable", host, port, user, pass)
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()
	return db

}
